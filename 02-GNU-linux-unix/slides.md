%title: Entretiens
%author: xavki
%blog: [Xavki Blog](https://xavki.blog)


# Que peut tu me dire sur GNU, Linux et Unix ?


<br>

Objectifs :

		* culture générale

		* comprendre l'environnement de travail

		* la passion de l'opensource


<br>

Multics OS

		* OS securisé

		* MIT, General Electric et Bell Labs

		* kernel et shell (couches, Onion architecture)

		* pb > coûts des ressources pour fonctionner

		* complexe > Bell Labs quitte le projet > UNIX (assembleur)

		* assembleur = s'imposait pour le dev des OS 

		* PL/1 sur Multics (démonstration de langage de plus haut niveau)


UNIX (ou Unix)

		* assembleur = s'imposait pour le dev des OS

		* recherches de langages adaptés pour le dev OS

		* 1969 langage B > Bell Labs (vient du BCPL Basic Combined Programming Language)

		* 1971 complexité du B > new B > C pour Unix


		

GNU > GNU is Not Unix

		* origine Richard Stallman

		* 
