%title: Entretiens
%author: xavki
%blog: [Xavki Blog](https://xavki.blog)


# Quelle est ta distribution favorite ? Pourquoi ?


<br>

Objectifs :

			* réaction

			* rigidité

			* passion


------------------------------------------------------------------------

# Quelle est ta distribution favorite ? Pourquoi ?


<br>

En avoir quelques unes en tête :

```
* Red Hat Enterprise Linux 
* Fedora 
* CentOS 
* Debian 
* Ubuntu 
* Linux Mint 
* SUSE 
* Arch Linux 
* Kali Lnux 
* Free BSD
...
```

------------------------------------------------------------------------------------

# Quelques idées de réponses

<br>

* Ubuntu :
		
		* simplicité de maintenance

		* bon desktop

		* de plus en plus utilisée côté serveur

		* garder le même environnement

		* convertir d'autres personnes à Linux

------------------------------------------------------------------------------------

# Quelques idées de réponses

<br>

* Fedora :

		* stabilité

		* environnement redhat

		* facilité de mise à jour 

------------------------------------------------------------------------------------

# Quelques idées de réponses

<br>

* Arch :

		* idéal pour apprendre

		* se limiter à ce dont vous avez besoin (paquets)

		* customiser en profondeur son environnement

		* wiki arch très instructif

		* indépendance dans la mise à jour des version

------------------------------------------------------------------------------------

# Quelques idées de réponses

<br>

* Linux Mint :

		* pour son interface graphique

		* les thèmes sont travaillés

		* ne pas avoir une ubuntu

		* simple d'utilisation


------------------------------------------------------------------------------------

# Ma réponse ??
